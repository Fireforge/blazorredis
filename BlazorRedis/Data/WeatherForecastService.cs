using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ServiceStack.Redis;

namespace BlazorRedis.Data
{
    public class WeatherForecastService
    {
        private static readonly string[] RedisHosts = new[] { "redis" };

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public static Task InitForecastAsync(DateTime startDate)
        {
            return Task.Run(() => {
                var rng = new Random();
                using (var redisManager = new PooledRedisClientManager(RedisHosts))
                using (var redis = redisManager.GetClient())
                {
                    var redisTodos = redis.As<WeatherForecast>();
                    foreach (var index in Enumerable.Range(1, 5))
                    {
                        redisTodos.Store(new WeatherForecast
                        {
                            Date = startDate.AddDays(index),
                            TemperatureC = rng.Next(-20, 55),
                            Summary = Summaries[rng.Next(Summaries.Length)]
                        });
                    }
                }
            });
        }

        public Task<WeatherForecast[]> GetForecastAsync()
        {
            return Task.Run(() => {
                using (var redisManager = new PooledRedisClientManager(RedisHosts))
                using (var redis = redisManager.GetClient())
                {
                    return redis.As<WeatherForecast>().GetAll().ToArray();
                }
            });
        }
    }
}
