FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
ARG APP_NAME=BlazorRedis
WORKDIR /app

# copy csproj and restore as distinct layers
COPY $APP_NAME/*.csproj .
RUN dotnet restore

# copy everything else and build app
COPY $APP_NAME/. .
RUN dotnet publish -c Release -o out


FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
COPY --from=build /app/out ./
ENTRYPOINT ["dotnet", "BlazorRedis.dll"]